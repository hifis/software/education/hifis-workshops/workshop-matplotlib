---
title: "Multiple Plots"
---

# More than one Plot

Often enough you want to actually plot more than one data set.
You have the choice wether you want all graphs in the same figure or one figure per graph, or a mixture in-between.

## Multiple Plots in the same Figure

Let us continue with the same example as in the previous section.
This time we add another set of data from the same river but at another year.

```python
from matplotlib import pyplot

months = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
]

water_levels_2010 = [
    5.77, 6.04, 6.52, 6.48, 6.54, 5.92, 
    5.64, 5.21, 5.01, 5.18, 5.45, 5.59
]

water_levels_2020 = [
    5.48, 5.82, 6.31, 6.26, 6.09, 5.87, 
    5.72, 5.54, 5.22, 4.86, 5.12, 5.40
]

pyplot.plot(months, water_levels_2010)
pyplot.plot(months, water_levels_2020)
pyplot.show()
```
In the case of multipe plots in one figure, it is very useful to also include a legend, so the viewer can distinguish which graph is which.

```python
# … Data as before

# Add labels to the plot function to determin how they show up in the legend
pyplot.plot(months, water_levels_2010, label="2010")
pyplot.plot(months, water_levels_2020, label="2020")

# And add in the legend itself
pyplot.legend()

pyplot.show()
```
!!! note "Best Practise"
    For any more complicated plot setup than these simple examples it is strongly recommended to use the object-oriented approach.
    This will be covered in a future lesson.

## Multiple separate Plots

If we want to have multiple dedicated plots in our figure, we can do so as well.
Let us plot the difference between the years in a separate drawing area.

```python
# … Data as before

# Calculate the difference
data_entry_count = len(water_levels_2020)
water_level_differences = [
    water_levels_2020[index] - water_levels_2010[index]
    for index in range(data_entry_count)
]  # (1)

# We want to arrange our plot in 2 rows and 1 column (i.e. have 2 plots)
figure, axes = pyplot.subplots(nrows=2, ncols=1) # (2)

# Select and create the first plot
pyplot.sca(axes[0])  # (3)
pyplot.title("Water levels in 2010 and 2020")
pyplot.plot(months, water_levels_2010, label="2010")
pyplot.plot(months, water_levels_2020, label="2020")
pyplot.legend()

# Select and create the second plot
pyplot.sca(axes[1])
pyplot.title("Difference between water levels")
pyplot.plot(months, water_level_differences)
pyplot.hlines(0, xmin=months[0], xmax=months[-1], color="black")

pyplot.show()
```

!!! note "Explanations"
    1. If you have not seen this kind of construction before: This is a so-called _list comprehension_.
       Think of it as a shortcut to write down a _for-loop_ that generates a list with a lot less clutter.
       The more exhaustive version would be
       ```python
       water_levels_differences = []
       for index in range(data_entry_count):
           water_levels_differences[index] = \
           water_levels_2020[index] - water_levels_2010[index]
       ```
    2. The imperative way of interacting with _pyplot_ needs to know which axes ( i.e. drawing area) you want to manipulate currently.
       _Matplotlib_'s solution is to return the figure and the various axes as the result of the 'subplots(…)'-function.
       In this case, `axes` has two elements, since we have two sub-plots that can be accesed via the respective indices.
    3. `sca(…)` stands for _set current axes_ and allows you to choose which drawing area you are currently manipulating. 
       It's counterpart would be `gca()` - _get current axes_.

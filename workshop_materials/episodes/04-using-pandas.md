---
title: "Matplotlib + Pandas"
---

# Combining _Matplotlib_ and _Pandas_

The _matplotlib_ framework integrates well with the popular _pandas_ data processing library.

!!! success
    If you would like to learn more about _pandas_, check out [our other workshop][hifis-pandas]!


For the following examples we will use our usual data:

```python
months = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
]

water_levels_2010 = [
    5.77, 6.04, 6.52, 6.48, 6.54, 5.92, 
    5.64, 5.21, 5.01, 5.18, 5.45, 5.59
]

water_levels_2020 = [
    5.48, 5.82, 6.31, 6.26, 6.09, 5.87, 
    5.72, 5.54, 5.22, 4.86, 5.12, 5.40
]
```

## Plotting _Pandas_ Series

To plot a _pandas_ series, it can be directly fed into the `pyplot.plot(…)`-function.

```python
from matplotlib import pyplot
from pandas import Series

# … Raw data as above

# We turn it into a series
measurements = Series(
    data = water_levels_2010,
    index=months,
    name="Water levels in 2010"
)

# And plot it
pyplot.plot(measurements, label=measurements.name)  # (1)
pyplot.legend()
pyplot.show()
```

!!! note "Explanation"
    1. Note how we can make use of the fact that the series has a name to also assign it as a plot label.

## Working with DataFrames

_Matplotlib_ can also handle _pandas_ `DataFrame`s as input:

```python
from pandas import DataFrame
from matplotlib import pyplot

# … Raw data as above

measurements = DataFrame(
    data = {
        "Water levels in 2010": water_levels_2010,
        "Water levels in 2020": water_levels_2020
    },
    index = months
)

pyplot.plot(measurements)
pyplot.legend(measurements.columns.values)
pyplot.show()
```

Data frames in _pandas_ actually also bring a plotting function with them, so you could also write it this way:

```python

# … Import and data frame creation as before

measurements.plot()
pyplot.show()
```

Isn't that convenient?
You can find more information on the [`DataFrame.plot` documentation page][pandas-plot-doc]

[hifis-pandas]: https://hifis.net/workshop-materials/python-pandas/
[pandas-plot-doc]: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.html


---
title: "Getting started with pyplot"
---

# Getting started with _pyplot_

The _matplotlib_ framework offers the module _pyplot_ as a set of convenient shortcuts for common plotting tasks.
_Pyplot_ also is the way through which users will mostly interact with the features of _matplotlib_.

## Creating simple plots

Imagine we are keeping track of a rivers' water level over a year:

```python
from matplotlib import pyplot  # We are going to need this

# Let's keep it simple and use a list for our measurement
# Average water level in meters for each month
water_levels = [
    5.77, 6.04, 6.52, 6.48, 6.54, 5.92, 
    5.64, 5.21, 5.01, 5.18, 5.45, 5.59
]

pyplot.plot(water_levels)  # This prepares a plot
pyplot.show()  # Show the plot that was created
```

Since we have not given any explicit _x_- axis values, _pyplot_ has used the index of our list for that purpose.

!!! note "Side note"
    _Pyplot_ internally keeps track of state of the plot. 
    You as the user only get to see it once you call the `show()` function.
    This way of programming is calles _imperative_.
    
    There is also a different approach to things, which is called _object oriented_ which is used in the underlying workings of _matplotlib_. 
    For clarity reasons we will show _pyplot_ first and then expand upon that a bit later.
    
    > To help with understanding, imagine you want to have a cake.
    > _Imperative_ style is like calling a baker and describing how you want your cake to look like.
    > _Object oriented_ style is like ordering the ingredients and then modifying the soon-to-be-cake yourself until it looks like you want it.

A simple way to plot something with specific _x_-values is to have separate lists of _x_ and _y_ values.
The _x_-values do not need to be numbers, category names work equally as well.

```python
from matplotlib import pyplot

months = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
]

water_levels = [
    5.77, 6.04, 6.52, 6.48, 6.54, 5.92, 
    5.64, 5.21, 5.01, 5.18, 5.45, 5.59
]

# Make sure the length of the x- and y-value lists match, 
# otherwise pyplot will complain

pyplot.plot(months, water_levels)
pyplot.show()
```
!!! note "Heads-up!"
    After calling `pyplot.show()`, _pyplot_ drops everything about the plot you just set up.
    If you find yourself recreating plots often, consider moving the plot creation into a function and putting it into a file to make your life a bit easier.

## Adding Details

We now have a very simple plot, but it is lacking a few things that we expect.
Let us add some basic things like labels, a title and maybe a better scale for the _y_-axis.

```python
# … Data as before

# First let's put in the overall decorations
pyplot.xlabel("Month")
pyplot.ylabel("[m]")
pyplot.title("Avg. water level of a river in 2022")

pyplot.plot(months, water_levels)

# We want our y-axis to start at 4m and go up to 8m with a resolution of 0.5m
water_level_marks = [4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0]
pyplot.yticks(water_level_marks)

pyplot.show()
```

For the viewers convenience we also want to add horizontal lines to indicate the minimum and maximum values of the water levels.

```python
# … Data as before

# … Title and labels as before

pyplot.plot(months, water_levels)

# Add the lines for the lowest and highest water level
marker_lines = [min(water_levels), max(water_levels)]
pyplot.hlines(
    y=marker_lines,
    xmin=months[0],  # (1)
    xmax=months[-1],
    linestyles="dotted",
    colors="lightgray"  # (2)
)

# … Set the tick marks as before

pyplot.show()
```

!!! note "Explanations"
    1. Note how we used the indices of the month list as the limits for the marker lines.
    The reasoning here is that if you change the month list (for example: reorder the months or change how they are written down) 
    the limits will adjust automatically.
    2. Regarding colors in _matplotlib_ you can refer to this [color specification](https://matplotlib.org/stable/tutorials/colors/colors.html).

!!! warning "Common Pitfall"
    The order in which you add the elements matter!
    If you put multiple elements into your plot, the last ones get plotted over the previous ones.
    Also if you plot the lines _after_ setting the tick markers, it will override the tick marker settings.
    In general, you can stick to the following order:

    1. Decorations, labels, title
    2. Plot elements from back to front
    3. Plot area scaling, transformations

    You can alternatively control the stacking order by passing a value for the `zorder` parameter into the plotting functions.
    

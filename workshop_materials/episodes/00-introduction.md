---
title: Introduction
---

# Introduction

## What is _Matplotlib_?
First things first: The name comes from _**Mat**rix **Plot**ting **Lib**rary_, not "Math…".

_Matplotlib_ ia a framework - i.e. a collection of functionality, not a program on its own.
Its main use case is generating data visualizations. 
The framework produces high quality images based on the data given and is very customizable.
It integrates well with other commonly used Python-frameworks like _numpy_ or _pandas_.

## How to get _matplotlib_?
It can be installed via the command line using _pip_:

```shell
pip install matplotlib
```

Graphical tools like _thonny_, _anaconda_ or _pycharm_ usually have a menu to install such packages.

## Where to find help?
All the important links and additional material is collected in the [_Further Reading_ section](../resources.md)

---

## Anatomy of a Plot

Knowing the right terms for the subject can help to better search for help, understand documentation and communicate issues and solutions.
In the following image you can find the naming of the elements in a plot as used by _matplotlib_:

![Picture: A plot with its elements marked](https://matplotlib.org/stable/_images/anatomy.png)

There are further terms that you may want to keep in mind when working with the framework.

!!! warning "Common Pitfall"
    In the nomenclature of the framework there is a difference between
    
    * **Axis:** the number line that gets printed on the side of the plot
    * **Axes:** the collection of all plotted elements that represent data (roughly: the plotting area)

### Artists and Figures

An **artist** determines the rendering style of plot elements.
The **figure** is the collection of all plot elements.
Figures can be nested into other figures. The nested element is called a _sub-figure_.
Implementation-wise, the _figure_ is an artist (i.e. it controls its own plotting style).
The rectangle for the figure's background is contained in the attribute `patch`.

# Introduction to Matplotlib

This is the writeup for a workshop to provide an introduction into the _Matplotlib_- framework.
The contents is divided in _episodes_ and _exercises_.
The episodes introduce the various core features of the framework, while the exercises can be tackled afterwards to apply those learned concepts to real-world cases and learn about potential caveats.

## Audience
This workshop is intended for learners interested in data science who have a basic understanding of working with Python like

* Variables, data types, functions
* Loops, conditionals

## Structure

The content is split into thematic episodes which are ordered along a story arc.
The episodes should be done first, followed by the exercises.

The exercises themselves are split into sections which contain one or multiple tasks to be solved.
Usually, there are many ways to solve these tasks and any solution that generates the desired output can be considered correct. 

## Contact

For any inquiries about this workshop please contact [the HIFIS support](mailto:support@hifis.net)


# Further Reading

This is a list of additional ressources that can help you to learn more about the _matplotlib_ framework.

## All the Important Links

* [Official Page](https://matplotlib.org/)
* [Documentation](https://matplotlib.org/stable/api/index)
* [Example gallery](https://matplotlib.org/stable/gallery/index.html)
* [_Matplotlib_ in the Python package index (_PyPi_)](https://pypi.org/project/matplotlib/)

## Plot Types

_Matplotlib_ supports a wide variety of plotting functions.
Here is a selection that is easily accessible through _pyplot_.

| Plot type    | `pyplot.` Function |
|--------------|--------------------|
| Line plot    | `plot()`           |
| Scatter plot | `scatter()`        |
| Bar plot     | `bar()`            |
| Box plot     | `boxplot()`        |
| Stem plot    | `stem()`           |
| Histogram    | `hist()`           |
| Polar plot   | `polar()`          |
| Matrix plot  | `imshow()`         |

See also:

* [List of _pyplot_ functions](https://matplotlib.org/stable/api/pyplot_summary.html)
* [_Matplotlib_ Examples Gallery](https://matplotlib.org/stable/gallery/index.html)


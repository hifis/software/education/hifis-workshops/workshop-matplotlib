---
title: "Creating a Simple Plot"
---

# Task 01: Creating a Simple Plot

Create a plot with all the basic elements.

### Data

The data you are supposed to plot are the outside temperatures over a day with an hourly resolution.

Here is some code you can copy & paste to get the data:

```python
# These are the hours of the day, nicely formatted as hh:00 in a 24h-format
hours = [f"{hour:02}:00" for hour in range(24)]

temperature = [
    25.6, 25.3, 25.1, 25.5,  # 00…03h
    26.0, 26.4, 27.4, 27.9,  # 04…07h
    28.8, 29.5, 30.4, 30.9,  # 08…11h
    31.5, 32.7, 33.8, 34.3,  # 12…15h
    34.4, 34.1, 33.2, 30.9,  # 16…19h
    29.3, 28.4, 27.1, 26.3   # 20…23h
]
```

## Now you!

1. Create a basic plot from the temperature data.
2. Assign the _x_-axis ticks so they show the hours of the day.
3. Assign the _y_-axis ticks so they show a range from 0 to 40.
4. Assign the _x_-axis label to indicate the meaning of the axis.
5. Assign the _y_-axis label to indicate the temperature is in `°C`.
6. Add a title of your own choice.

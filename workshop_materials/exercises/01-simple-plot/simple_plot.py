from matplotlib import pyplot

# These are the hours of the day, nicely formatted as hh:00 in a 24h-format
hours = [f"{hour:02}:00" for hour in range(24)]

temperature = [
    25.6, 25.3, 25.1, 25.5,  # 00…03h
    26.0, 26.4, 27.4, 27.9,  # 04…07h
    28.8, 29.5, 30.4, 30.9,  # 08…11h
    31.5, 32.7, 33.8, 34.3,  # 12…15h
    34.4, 34.1, 33.2, 30.9,  # 16…19h
    29.3, 28.4, 27.1, 26.3   # 20…23h
]

pyplot.plot(hours, temperature)
pyplot.xticks(hours[::3])
pyplot.yticks(range(0, 40, 5))
pyplot.xlabel("Hour of Day")
pyplot.ylabel("Temperature [°C]")

pyplot.show()

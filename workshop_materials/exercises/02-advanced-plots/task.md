---
title: "Advanced Plotting"
---

# Task 02: Advanced Plotting

### Data

Here is some data from a ground cross-section on the sides of a river bed.
This time the data is given as a _pandas_ data frame.

```python
from pandas import DataFrame

# The data frame gives the elevation above/below the water level
# for the top of each ground type.
# The index is the position along the measured cross-cut
# All values are given in meters

layers = DataFrame(
    { 
        "topsoil": [
            6.54, 5.98, 5.23, 3.21, 0.14, -2.12, -5.48, -6.33, -6.91, -6.42,
            -5.64, -2.98, -0.41, 2.82, 4.23, 5.79, 5.99
        ],
        "clay": [
            5.34, 4.21, 3.53, 1.00, -0.56, -2.12, -5.48, -6.33, -6.91, -6.42,
            -5.64, -2.98, -0.47, 1.56, 2.06, 4.32, 4.87
        ],
        "bedrock": [
            1.24, 1.10, 0.65, -0.21, -1.33, -2.98, -5.48, -6.33, -6.91, -6.42,
            -5.64, -3.67, -3.45, -3.01, -2.54, -1.59, 0.65
        ]
    }, 
    index = range(-40, 41, 5)
)
```

### Now you!

1. Plot the cross-sections of the various ground layers
    * The _topsoil_ is to be plotted in dark green
    * The _clay_ is to be plotted in a yellowish-brown tone
    * The _bedrock_ is to be plotted in a dark gray tone
    * Optional: fill the areas below each layer with an appropriate color as well.
2. Indicate the water level with a thick solid blue line at `y = 0`
3. Find the x-axis values where the _topsoil_ layer meets the water line. Mark those with a dashed line from the water level down to the x-axis.
4. Fill the area between these markers and the curve below them in a blue colored hatching to indicate the river.
5. Add a grid with a resolution of _5m × 5m_ to aid in orientation. Set the `alpha`-parameter of the grid to `0.5` to make it semi-transparent.
6. Add text on the left side of the river to indicate the names of the soil layers.

!!! note "Hints"
    Here is a list of named colors available in _matplotlib_:
    ![A table with colors and their names](https://matplotlib.org/1.4.2/mpl_examples/color/named_colors.hires.png)

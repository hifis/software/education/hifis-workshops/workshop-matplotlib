
categories = ["Crime", "Drama", "Fiction", "Romance", "Science"] 

# Book rentals for each category classified by popularity for a month 
books_rented_never = [13, 11, 7, 10, 5] 
books_rented_once = [15, 12, 13, 11, 16] 
books_rented_multiple = [12, 20, 15, 11, 12]

popularities = [
    books_rented_never,
    books_rented_once,
    books_rented_multiple
]
popularity_labels = ["Never", "Once", "Multiple"]

stacked_bars_top = [0, 0, 0, 0, 0] # Start at the 0 line

for index, popularity in enumerate(popularities):
    pyplot.bar(
        categories, popularity,
        label=popularity_labels[index],
        bottom = stacked_bars_top
    )
    # update the stacked_bars_top 
    # to have a new bottom for the coming categories
    stacked_bars_top = [
        sum(values) for values in zip(stacked_bars_top, popularity)
    ]

pyplot.legend()
pyplot.show()


from matplotlib import pyplot

artemis_hits = [
    (-1.3, -6.22), (-0.63, 0.59), (0.46, -8.34), (4.08, 0.52), (8.32, 3.73),
    (-2.96, 4.09), (-0.78, 7.81), (-2.19, 2.97), (-7.24, 1.43), (-0.54, 7.69)
]
robin_hits = [
    (7.73, -4.41), (3.71, -0.52), (2.76, -4.99), (3.33, -6.93), (5.32, 0.66),
    (4.19, -2.93), (5.76, -6.96), (8.69, -3.47), (2.0, -7.76), (5.15, -4.62)
]

merida_hits = [
    (-0.02, 5.35), (-4.0, -0.44), (1.47, -0.46), (-0.04, -0.29), (-1.62, 0.93),
    (-6.06, 1.13), (4.91, -5.17), (1.34, -0.09), (-2.17, -1.41), (-0.26, 4.58)
]

hou_yi_hits =[
    (6.76, 1.82), (5.95, 2.02), (5.95, 2.23), (3.22, 2.67), (5.92, 0.73),
    (2.41, 1.96), (5.0, 1.29), (5.42, 2.93), (3.08, 1.19), (5.11, 3.52)
]

labels = ["Artemis", "Robin", "Merida", "Hou Yi"]

origin = (0, 0)
axis_range = range(-21, 22, 3)

axes = pyplot.gca()
axes.axis("equal")

target = pyplot.Circle(origin, 20, color="lightgray")
axes.add_patch(target)
for radius in [1, 4, 8, 15]:
    ring = pyplot.Circle(origin, radius, fill=False, color="gray")
    axes.add_patch(ring)

for index, hits in enumerate([artemis_hits, robin_hits, merida_hits, hou_yi_hits]):
    axes.scatter(*list(zip(*hits)), label=labels[index], zorder=2)

pyplot.xticks(axis_range)
pyplot.yticks(axis_range)
pyplot.legend()

pyplot.show()

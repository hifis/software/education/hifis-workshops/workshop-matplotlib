
from math import pi
from matplotlib import pyplot

# Data
directions = [angle for angle in range(0, 360, 10)]  # 0°, 10°, 20°, …, 350°
wind_speeds = [
    1.07, 2.07, 2.47, 3.32, 5.22, 6.22,        #   0° …  50°
    6.19, 7.08, 7.12, 8.33, 8.91, 8.85,        #  60° … 110°
    9.94, 10.71, 10.96, 10.71, 10.83, 10.03,   # 120° … 170°
    10.87, 10.71, 11.69, 10.59, 11.21, 10.65,  # 180° … 230°
    9.38, 9.13, 8.05, 7.77, 7.99, 6.06,        # 240° … 290°
    6.24, 5.25, 3.62, 2.92, 2.93, 2.41         # 300° … 350°
]

# Add the first elements to the lists' ends to close the plot
directions.append(direction[0])
wind_speeds.append(wind_speeds[0])

# Translate the directions to radians
directions_radians = [(angle * pi) / 180 for angle in directions]

pyplot.polar(directions_radians, wind_speeds)

# Manipulate the axis to rotate and flip the plot
axes = pyplot.gca()
axes.set_theta_zero_location("N")
axes.set_theta_direction(-1)

# Done
pyplot.show()

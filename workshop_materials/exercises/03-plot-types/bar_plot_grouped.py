
from matplotlib import pyplot

# Data
categories = ["Crime", "Drama", "Fiction", "Romance", "Science"]

# Book rentals for each category classified by popularity for a month
books_rented_never = [13, 11, 7, 10, 5]
books_rented_once = [15, 12, 13, 11, 16]
books_rented_multiple = [12, 20, 15, 11, 12]

# Helpers for plotting

category_indexes = range(len(categories))

populatities = [
    books_rented_never,
    books_rented_once,
    books_rented_multiple
]
popularity_labels = ["Never", "Once", "Multiple"]

# Configure the bar sizes
bar_width = 0.2  # Same bar, left to right
bar_distance = 0.25  # From this bars' left to the next bars' left
category_distance = 1  # From this groupings' left to the next groupings' left

# The bars get grouped by category (i.e. the literary genres),
# so there is one bar of each popularity per group.
# Since the data is grouped by popularity we have to skip around
# across the groupings when determining the bar locations
#
# The index in the `popularities` list of the current popularity
# is the position of a bar within the category-group
for offset_in_group, popularity in enumerate(populatities):
    popularity_indexes = range(len(popularity))
    bar_x_locations = [  # x-positions of the bars of the current popularity
        category_index * category_distance + offset_in_group * bar_distance  
        for category_index in popularity_indexes
    ]
    pyplot.bar(
        bar_x_locations, popularity, 
        width=bar_width, 
        label=popularity_labels[offset_in_group]
    )

pyplot.xticks(category_indexes, categories)
pyplot.legend()
pyplot.show()
